var username = document.getElementById("user").value;
var password = document.getElementById("pass").value;
var basicAuthPass = btoa(username + ":" + password); //base64 enconding
var jql = document.getElementById("sprintName").value;
var encondedJql = encodeURI(jql);


var url = "/rest/api/latest/search?jql=+" + encondedJql;
console.log(url);
var taskList = document.getElementById("tasklist");
var status = document.getElementById("status");
var requestHeaders = new Headers();
requestHeaders.append("Content-Type", "application/json");
requestHeaders.append("Authorization", "Basic " + basicAuthPass);

requestHeaders.append('Access-Control-Allow-Origin', '*');
//requestheader.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
requestHeaders.append("Accept", 'application/json');
console.log("Headerrr");
console.log(requestHeaders);
var initConfig = {
  method: "GET",
  headers: requestHeaders
};
console.log("init");
console.log(requestHeaders);

var createIssueCard = function createIssueCard(issuetype, key, summary, points, hours) {
  return "\n  <li class=\"task\">\n    "
          + "    <div class=\"card_header card_header_"+ issuetype + "\">\n" 
          + "      <div class=\"logo\"></div>\n      <span>" + key + "</span>\n    </div>\n" 
          + "    <div class=\"summary\">" + summary + "</div>\n" 
          + "    <div class=\"points\">" + points + "</div>\n" 
          + "    <div class=\"hours\">" + hours + "</div>\n"
          + "  </li>";
};

fetch(url, initConfig).then(function (response) {
  if (response.status !== 200) {
    alert("");
    status.innerHTML += "Uh-oh looks like we have" + "error [" + response.status + "] -  " + response.statusText + ". Hit refresh and try again with valid credentials";
  } else {
    response.json().then(function (data) {
      data.issues.forEach(function (issue) {
        taskList.innerHTML += createIssueCard(getIssueType(issue.fields.issuetype.name), issue.key
                              , getIssueSummary(issue.fields.summary), getStoryPoints(issue.fields.customfield_10014)
                              , getOriginalEstimate(issue.fields.timeestimate));
      });
    });
  }
}).catch(function (error) {
  status.innerHTML += "\n" + error;
});

function getIssueType(name){
  if(name == 'Technical task'){
    return "techtask";
  } else if(name){
    return name;
  }
  return "default";
}

function getIssueSummary(summary){
  return summary ? summary : "";
}

function getStoryPoints(storyPoints){
  return storyPoints ? storyPoints : "";
}

function getOriginalEstimate(time){
  return time ? time/3600 : "";
}