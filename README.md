# Print Jira Cards

This application creates Jira cards on an HTML page which then can be printed on paper and used on physical board.
One has to provide userid, password of Jira and specify the JIRA url in nginx.conf

## How to run the application
* Install nginx reverse proxy to avoid CORS issue.

* Configure nginx.conf according to your JIRA installation.

* To start nginx on the current directory

    `nginx -p . -c nginx.conf`

* To stop nginx on the current directory

    `nginx -p . -s stop`

* In order to run the application use http://localhost:8081 url
